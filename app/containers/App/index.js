/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import Illness from 'containers/Illness';
import Symptoms from 'containers/Symptoms';
import Category from 'containers/Category';

import NotFoundPage from 'containers/NotFoundPage/Loadable';

import Nav from '../../components/Navigation';

import GlobalStyle from '../../global-styles';

const AppWrapper = styled.div`
  display: flex;
  height: 100%;
`;

export default function App() {
  return (
    <AppWrapper>
      <Helmet
        titleTemplate="%s - React.js Boilerplate"
        defaultTitle="React.js Boilerplate"
      >
        <meta name="description" content="A React.js Boilerplate application" />
      </Helmet>
      <Nav />
      <Switch>
        <Route exact path="/" component={Illness} />
        <Route exact path="/symptoms" component={Symptoms} />
        <Route exact path="/category" component={Category} />
        <Route path="" component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </AppWrapper>
  );
}
