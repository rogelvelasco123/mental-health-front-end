/* eslint-disable no-underscore-dangle */
/*
 * FeaturePage
 *
 * List all the features
 */
import React, { useState, useEffect } from 'react';
import { Button, Table } from 'antd';
import styled from 'styled-components';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import CategoryDrawer from 'components/CategoryDrawer';
import GlobalStyle from '../../global-styles';

const MainContainer = styled.div`
  background: #ececec;
  display: flex;
  width: 100%;
  flex-direction: column;

  .table-container {
    padding: 15px;
  }
  .table-actions {
    .edit-icon {
      margin-right: 5px;
      font-size: 16px;
      color: #367fa9;
      cursor: pointer;
    }
    .delete-icon {
      font-size: 16px;
      color: red;
      cursor: pointer;
    }
  }
`;

const Header = styled.div`
  background: #fff;
  box-shadow: 0px 1px 2px hsla(0, 0%, 0%, 0.2);
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  .header-title {
    font-size: 20px;
    font-weight: bold;
    padding: 10px 10px 10px 20px;
  }
  .button-container {
  }
`;

const StyledButton = styled(Button)`
  border-radius: 0 !important;
  height: 100% !important;
  font-size: 16px !important;
  font-weight: bold !important;
  background: #367fa9 !important;
  border: 0 !important;

  .plus-icon {
    margin-right: 5px;
  }
`;

const StyledTable = styled(Table)`
  margin: 8px 0px;
  background: white;
  width: 100%;

  th {
    font-size: 12px !important;
    line-height: 14px !important;
    font-weight: bold !important;
    text-align: center !important;
    text-transform: uppercase;
    white-space: nowrap;

    /* span{
      display: flex;
      justify-content: center;
      align-content: center;
    } */
  }

  td {
    text-align: center !important;
    font-size: 14px !important;
    line-height: 16px;
    color: #555555;
    padding: 6px !important;
  }

  .ant-table-tbody > tr:hover > td {
    cursor: pointer;
    /* background: #74b9ff55; */
    color: #555555;
    a {
      color: #555555;
    }
  }

  .ant-table-small
    > .ant-table-content
    > .ant-table-body
    > table
    > .ant-table-thead
    > tr
    > th {
    color: #0984e3;
  }

  tr:nth-child(odd) {
    background: #fafafa;
  }

  .ant-table-body {
    overflow-x: auto !important;

    ::-webkit-scrollbar-track {
      box-shadow: inset 0 0 8px rgba(0, 0, 0, 0.3);
      background-color: #dfe6e9;
    }
    ::-webkit-scrollbar {
      height: 4px;
      background-color: #7f8c8d;
    }

    ::-webkit-scrollbar-thumb {
      box-shadow: inset 0 0 8px rgba(0, 0, 0, 0.3);
      background-color: #7f8c8d;
    }
  }
`;

const Illness = () => {
  const [drawerVisible, setDrawerVisible] = useState(false);
  const [illnessData, setIllnessData] = useState([]);
  const [forUpdate, setForUpdate] = useState({});
  const [CategoryName, setCategoryName] = useState('');
  const [CategoryUrl, setCategoryUrl] = useState('');
  const [update, setUpdate] = useState(false);

  const handleCategoryName = data => {
    setCategoryName(data);
  };

  const handleCategoryUrl = data => {
    setCategoryUrl(data);
  };

  const handleUpdate = data => {
    setUpdate(data);
  };

  const onDelete = (key, e) => {
    setIllnessData(illnessData.filter(item => item._id !== key));
    axios.delete(`https://api.chkmentalhealth.site/category/${key}`);
  };

  const onUpdate = record => {
    setDrawerVisible(true);
    setCategoryName(record.name);
    handleCategoryUrl(record.imagePath);
    setUpdate(true);
    setForUpdate(record);
  };

  const columns = [
    {
      title: 'Category Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Image Path',
      dataIndex: 'imagePath',
      key: 'imagePath',
    },
    {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 100,
      render: (text, record) => (
        <div className="table-actions">
          <FontAwesomeIcon
            className="edit-icon"
            icon={faEdit}
            onClick={() => onUpdate(record)}
          />
          <FontAwesomeIcon
            onClick={() => onDelete(record._id)}
            className="delete-icon"
            icon={faTrashAlt}
          />
        </div>
      ),
    },
  ];
  const handleClick = () => {
    setDrawerVisible(true);
  };
  const handleClose = () => {
    setDrawerVisible(false);
    setCategoryName('');
    handleCategoryUrl('');
  };

  const getCategoryData = () => {
    axios.get('https://api.chkmentalhealth.site/category').then(res => {
      setIllnessData(res.data.reverse());
    });
  };

  const updateTable = data => {
    setIllnessData(data.reverse());
  };

  useEffect(() => {
    getCategoryData();
  }, []);
  return (
    <MainContainer>
      <Header>
        <div className="header-title">Category Lists</div>
        <div className="button-container">
          <StyledButton type="primary" onClick={handleClick}>
            <FontAwesomeIcon icon={faPlus} fixedWidth className="plus-icon" />
            Add New Category{' '}
          </StyledButton>
        </div>
      </Header>
      <div className="table-container">
        <StyledTable
          columns={columns}
          dataSource={illnessData}
          pagination={{ pageSize: 10 }}
          rowKey={record => record._id}
        />
      </div>

      <CategoryDrawer
        visible={drawerVisible}
        onClose={handleClose}
        getIllnessData={updateTable}
        CategoryName={CategoryName}
        CategoryUrl={CategoryUrl}
        handleCategoryName={handleCategoryName}
        handleCategoryUrl={handleCategoryUrl}
        update={update}
        handleUpdate={handleUpdate}
        forUpdate={forUpdate}
      />
      <GlobalStyle />
    </MainContainer>
  );
};

export default Illness;
