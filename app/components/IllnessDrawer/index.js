/* eslint-disable no-unused-expressions */
/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState } from 'react';
import { Drawer, Input, Select, Button, notification } from 'antd';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons';

const StyledDrawer = styled(Drawer)`
  .ant-drawer-header {
    background: #367fa9;
    border-radius: 0;
  }
  .ant-drawer-title {
    color: #fff;
    font-size: 18px;
    font-weight: bold;
  }
  .ant-drawer-close {
    color: #fff;
    margin-top: -3px;
  }
  .ant-drawer-wrapper-body {
    background: #ececec;
  }
  .ant-drawer-body {
    padding: 15px;
  }
  .plus-icon {
    margin-right: 6px;
  }

  .input-container {
    display: flex;
    flex-direction: column;
    margin-bottom: 12px;
  }
  .label {
    font-size: 16px;
    line-height: 18px;
    font-weight: bold;
    margin-bottom: 6px;
  }
  .isRequired {
    color: red;
  }
`;

const StyledInput = styled(Input)``;
const { Option } = Select;
const { TextArea } = Input;
const IllnessDrawer = props => {
  const [symptomsData, setSymptomsData] = useState([]);

  useEffect(() => {
    axios
      .get('https://api.chkmentalhealth.site/symptoms')
      .then(res => setSymptomsData(res.data));
  }, []);

  const handleIllnessUpdate = id => {
    if (
      props.illnessName === '' ||
      props.illnessSymptoms.length === 0 ||
      props.illnessCause === '' ||
      props.illnessRecommendation === ''
    ) {
      notification.error({
        message: `Error`,
        description: 'Please fill up all the required fields',
        placement: 'topLeft',
      });
    } else {
      const obj = {
        name: props.illnessName,
        symptoms: props.illnessSymptoms,
        cause: props.illnessCause,
        recommendation: props.illnessRecommendation,
      };

      axios
        .patch(`https://api.chkmentalhealth.site/illness/${id}`, obj)
        .then(() => {
          notification.success({
            message: `Success`,
            description: `Updated successfully`,
            placement: 'topLeft',
          });
          axios
            .get('https://api.chkmentalhealth.site/illness')
            .then(resp => props.getIllnessData(resp.data));
        });
      props.handleUpdate(false);
      props.onClose();
      props.handleIllnessName('');
      props.handleIllnessSymptoms([]);
      props.handleIllnessCause('');
      props.handleIllnessRecommendation('');
    }
  };

  const handleSubmit = () => {
    if (
      props.illnessName === '' ||
      props.illnessSymptoms.length === 0 ||
      props.illnessCause === '' ||
      props.illnessRecommendation === ''
    ) {
      notification.error({
        message: `Error`,
        description: 'Please fill up all the required fields',
        placement: 'topLeft',
      });
    } else {
      const obj = {
        name: props.illnessName,
        symptoms: props.illnessSymptoms,
        cause: props.illnessCause,
        recommendation: props.illnessRecommendation,
      };

      axios.post('https://api.chkmentalhealth.site/illness', obj).then(res => {
        notification.success({
          message: `Success`,
          description: `${res.data.name} successfully added in Illness`,
          placement: 'topLeft',
        });
        axios
          .get('https://api.chkmentalhealth.site/illness')
          .then(resp => props.getIllnessData(resp.data));
      });
      props.handleIllnessName('');
      props.handleIllnessSymptoms([]);
      props.handleIllnessCause('');
      props.handleIllnessRecommendation('');
      props.onClose();
    }
  };

  return (
    <StyledDrawer
      destroyOnClose
      width={500}
      title={
        <div className="drawer-title">
          <FontAwesomeIcon
            icon={faPlusSquare}
            fixedWidth
            className="plus-icon"
          />
          ADD NEW ILLNESS
        </div>
      }
      closable
      onClose={props.onClose}
      visible={props.visible}
      placement="right"
    >
      <div className="input-container">
        <div className="label">
          <span className="isRequired">*</span> Illness Name
        </div>
        <StyledInput
          defaultValue={props.illnessName}
          size="large"
          onChange={e => props.handleIllnessName(e.target.value)}
        />
      </div>
      <div className="input-container">
        <div className="label">
          <span className="isRequired">*</span> Symptoms
        </div>
        <Select
          defaultValue={props.illnessSymptoms}
          size="large"
          mode="multiple"
          onChange={value => props.handleIllnessSymptoms(value)}
          optionLabelProp="label"
          allowClear
        >
          {symptomsData.map(data => (
            <Option key={data.name} label={data.name}>
              {data.name} - {data.category}
            </Option>
          ))}
        </Select>
      </div>
      <div className="input-container">
        <div className="label">
          <span className="isRequired">*</span> Cause
        </div>
        <TextArea
          defaultValue={props.illnessCause}
          rows={6}
          onChange={e => props.handleIllnessCause(e.target.value)}
        />
      </div>
      <div className="input-container">
        <div className="label">
          <span className="isRequired">*</span> Recommendation
        </div>
        <TextArea
          defaultValue={props.illnessRecommendation}
          rows={4}
          onChange={e => props.handleIllnessRecommendation(e.target.value)}
        />
      </div>
      <div
        style={{
          position: 'absolute',
          right: 0,
          bottom: 0,
          width: '100%',
          borderTop: '1px solid #e9e9e9',
          padding: '10px 16px',
          background: '#ececec',
          textAlign: 'right',
        }}
      >
        <Button onClick={props.onClose} style={{ marginRight: 8 }}>
          Cancel
        </Button>
        <Button
          type="primary"
          onClick={
            props.update
              ? () => handleIllnessUpdate(props.forUpdate._id)
              : handleSubmit
          }
        >
          Submit
        </Button>
      </div>
    </StyledDrawer>
  );
};

IllnessDrawer.propTypes = {
  onClose: PropTypes.func,
  visible: PropTypes.bool,
  getIllnessData: PropTypes.func,
  illnessName: PropTypes.string,
  illnessSymptoms: PropTypes.array,
  illnessCause: PropTypes.string,
  illnessRecommendation: PropTypes.string,
  handleIllnessName: PropTypes.func,
  handleIllnessSymptoms: PropTypes.func,
  handleIllnessCause: PropTypes.func,
  handleIllnessRecommendation: PropTypes.func,
  update: PropTypes.bool,
  handleUpdate: PropTypes.func,
  forUpdate: PropTypes.object,
};

export default IllnessDrawer;
