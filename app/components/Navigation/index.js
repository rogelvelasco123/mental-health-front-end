/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { Menu, Icon } from 'antd';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faDiagnoses,
  faListAlt,
  faUserInjured,
} from '@fortawesome/free-solid-svg-icons';

const MainContainer = styled.div`
  width: 250px;
  background: #1e282c;
  height: 100%;
  display: flex;
  flex-direction: column;

  .app-name-container {
    background: #367fa9;
    height: auto;
    display: flex;
    padding: 20px 15px;
    justify-content: center;
    color: #fff;
    font-size: 20px;
    font-weight: 700;
  }
`;

const MenuItem = styled.div`
  color: ${props => (props.selected ? '#fff' : 'rgba(255,255,255,0.8)')};
  padding: 24px 15px;
  border-left: ${props =>
    props.selected ? '4px solid #fff' : '4px solid transparent'};
  font-size: 18px;
  line-height: 20px;
  .menu-icon {
    margin-right: 6px;
  }
  font-weight: ${props => (props.selected ? '700' : '500')};
  background: ${props =>
    props.selected ? 'rgba(255,255,255,0.05)' : 'transparent'};
`;

const Navigation = props => {
  const [selectedIllness, setSelectedIllness] = useState(true);
  const [selectedSymptoms, setSelectedSymptoms] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState(false);

  const handleIllness = () => {
    props.history.push('/');
    setSelectedIllness(true);
    setSelectedSymptoms(false);
    setSelectedCategory(false);
  };
  const handleSymptoms = () => {
    props.history.push('/symptoms');
    setSelectedIllness(false);
    setSelectedSymptoms(true);
    setSelectedCategory(false);
  };
  const handleCategory = () => {
    props.history.push('/category');
    setSelectedIllness(false);
    setSelectedSymptoms(false);
    setSelectedCategory(true);
  };
  return (
    <MainContainer>
      <div className="app-name-container">CheckMate</div>
      <MenuItem
        className="menu-item"
        onClick={handleIllness}
        selected={selectedIllness}
      >
        <FontAwesomeIcon icon={faUserInjured} className="menu-icon" /> Illness
      </MenuItem>
      <MenuItem
        className="menu-item"
        onClick={handleSymptoms}
        selected={selectedSymptoms}
      >
        <FontAwesomeIcon icon={faDiagnoses} className="menu-icon" /> Symptoms
      </MenuItem>
      <MenuItem
        className="menu-item"
        onClick={handleCategory}
        selected={selectedCategory}
      >
        <FontAwesomeIcon icon={faListAlt} className="menu-icon" /> Category
      </MenuItem>
    </MainContainer>
  );
};

export default withRouter(Navigation);
